<?php
App::uses('AppController', 'Controller');
/**
 * Certifications Controller
 *
 * @property Certification $Certification
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class CertificationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Certification->recursive = 0;
		$this->set('certifications', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Certification->exists($id)) {
			throw new NotFoundException(__('Invalid certification'));
		}
		$options = array('conditions' => array('Certification.' . $this->Certification->primaryKey => $id));
		$this->set('certification', $this->Certification->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Certification->create();
			if ($this->Certification->save($this->request->data)) {
				$this->Flash->success(__('The certification has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The certification could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Certification->exists($id)) {
			throw new NotFoundException(__('Invalid certification'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Certification->save($this->request->data)) {
				$this->Flash->success(__('The certification has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The certification could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Certification.' . $this->Certification->primaryKey => $id));
			$this->request->data = $this->Certification->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->Certification->exists($id)) {
			throw new NotFoundException(__('Invalid certification'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Certification->delete($id)) {
			$this->Flash->success(__('The certification has been deleted.'));
		} else {
			$this->Flash->error(__('The certification could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
