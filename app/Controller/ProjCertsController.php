<?php
App::uses('AppController', 'Controller');
/**
 * ProjCerts Controller
 *
 * @property ProjCert $ProjCert
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ProjCertsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjCert->recursive = 0;
		$this->set('projCerts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjCert->exists($id)) {
			throw new NotFoundException(__('Invalid proj cert'));
		}
		$options = array('conditions' => array('ProjCert.' . $this->ProjCert->primaryKey => $id));
		$this->set('projCert', $this->ProjCert->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjCert->create();
			if ($this->ProjCert->save($this->request->data)) {
				$this->Flash->success(__('The proj cert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The proj cert could not be saved. Please, try again.'));
			}
		}
		$projects = $this->ProjCert->Project->find('list');
		$certifications = $this->ProjCert->Certification->find('list');
		$this->set(compact('projects', 'certifications'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjCert->exists($id)) {
			throw new NotFoundException(__('Invalid proj cert'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjCert->save($this->request->data)) {
				$this->Flash->success(__('The proj cert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The proj cert could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjCert.' . $this->ProjCert->primaryKey => $id));
			$this->request->data = $this->ProjCert->find('first', $options);
		}
		$projects = $this->ProjCert->Project->find('list');
		$certifications = $this->ProjCert->Certification->find('list');
		$this->set(compact('projects', 'certifications'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->ProjCert->exists($id)) {
			throw new NotFoundException(__('Invalid proj cert'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjCert->delete($id)) {
			$this->Flash->success(__('The proj cert has been deleted.'));
		} else {
			$this->Flash->error(__('The proj cert could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
