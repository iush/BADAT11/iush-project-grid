<div class="news view">
<h2><?php echo __('News'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($news['News']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($news['News']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($news['News']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($news['News']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($news['News']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit News'), array('action' => 'edit', $news['News']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete News'), array('action' => 'delete', $news['News']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $news['News']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List News'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Projects'); ?></h3>
	<?php if (!empty($news['Project'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Org Id'); ?></th>
		<th><?php echo __('Discipline Id'); ?></th>
		<th><?php echo __('New Id'); ?></th>
		<th><?php echo __('Teacher Project Posted Sequence'); ?></th>
		<th><?php echo __('Project Type'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Project Essay'); ?></th>
		<th><?php echo __('Project Subject Category Tree'); ?></th>
		<th><?php echo __('Project Subject Subcategory Tree'); ?></th>
		<th><?php echo __('Project Grade Level Category'); ?></th>
		<th><?php echo __('Project Resource Category'); ?></th>
		<th><?php echo __('Project Cost'); ?></th>
		<th><?php echo __('Project Posted Date'); ?></th>
		<th><?php echo __('Project Current Status'); ?></th>
		<th><?php echo __('Project Fully Funded Date'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($news['Project'] as $project): ?>
		<tr>
			<td><?php echo $project['id']; ?></td>
			<td><?php echo $project['org_id']; ?></td>
			<td><?php echo $project['discipline_id']; ?></td>
			<td><?php echo $project['new_id']; ?></td>
			<td><?php echo $project['Teacher_Project_Posted_Sequence']; ?></td>
			<td><?php echo $project['Project_Type']; ?></td>
			<td><?php echo $project['title']; ?></td>
			<td><?php echo $project['Project_Essay']; ?></td>
			<td><?php echo $project['Project_Subject_Category_Tree']; ?></td>
			<td><?php echo $project['Project_Subject_Subcategory_Tree']; ?></td>
			<td><?php echo $project['Project_Grade_Level_Category']; ?></td>
			<td><?php echo $project['Project_Resource_Category']; ?></td>
			<td><?php echo $project['Project_Cost']; ?></td>
			<td><?php echo $project['Project_Posted_Date']; ?></td>
			<td><?php echo $project['Project_Current_Status']; ?></td>
			<td><?php echo $project['Project_Fully_Funded_Date']; ?></td>
			<td><?php echo $project['created']; ?></td>
			<td><?php echo $project['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'projects', 'action' => 'view', $project['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'projects', 'action' => 'edit', $project['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'projects', 'action' => 'delete', $project['id']), array('confirm' => __('Are you sure you want to delete # %s?', $project['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
