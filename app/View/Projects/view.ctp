<div class="projects view">
<h2><?php echo __('Project'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($project['Project']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Org'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Org']['name'], array('controller' => 'orgs', 'action' => 'view', $project['Org']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Discipline'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Discipline']['name'], array('controller' => 'disciplines', 'action' => 'view', $project['Discipline']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['New']['name'], array('controller' => 'news', 'action' => 'view', $project['New']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Teacher Project Posted Sequence'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Teacher_Project_Posted_Sequence']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Type'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($project['Project']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Essay'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Essay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Subject Category Tree'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Subject_Category_Tree']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Subject Subcategory Tree'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Subject_Subcategory_Tree']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Grade Level Category'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Grade_Level_Category']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Resource Category'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Resource_Category']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Cost'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Posted Date'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Posted_Date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Current Status'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Current_Status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Fully Funded Date'); ?></dt>
		<dd>
			<?php echo h($project['Project']['Project_Fully_Funded_Date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($project['Project']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($project['Project']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project'), array('action' => 'edit', $project['Project']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project'), array('action' => 'delete', $project['Project']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $project['Project']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Orgs'), array('controller' => 'orgs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Org'), array('controller' => 'orgs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Disciplines'), array('controller' => 'disciplines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Discipline'), array('controller' => 'disciplines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List News'), array('controller' => 'news', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New New'), array('controller' => 'news', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Certs'), array('controller' => 'proj_certs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Cert'), array('controller' => 'proj_certs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Colls'), array('controller' => 'proj_colls', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Coll'), array('controller' => 'proj_colls', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Proj Certs'); ?></h3>
	<?php if (!empty($project['ProjCert'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Certification Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($project['ProjCert'] as $projCert): ?>
		<tr>
			<td><?php echo $projCert['id']; ?></td>
			<td><?php echo $projCert['name']; ?></td>
			<td><?php echo $projCert['project_id']; ?></td>
			<td><?php echo $projCert['certification_id']; ?></td>
			<td><?php echo $projCert['created']; ?></td>
			<td><?php echo $projCert['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proj_certs', 'action' => 'view', $projCert['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proj_certs', 'action' => 'edit', $projCert['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proj_certs', 'action' => 'delete', $projCert['id']), array('confirm' => __('Are you sure you want to delete # %s?', $projCert['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Cert'), array('controller' => 'proj_certs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Proj Colls'); ?></h3>
	<?php if (!empty($project['ProjColl'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Collaborator Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($project['ProjColl'] as $projColl): ?>
		<tr>
			<td><?php echo $projColl['id']; ?></td>
			<td><?php echo $projColl['name']; ?></td>
			<td><?php echo $projColl['project_id']; ?></td>
			<td><?php echo $projColl['collaborator_id']; ?></td>
			<td><?php echo $projColl['created']; ?></td>
			<td><?php echo $projColl['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proj_colls', 'action' => 'view', $projColl['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proj_colls', 'action' => 'edit', $projColl['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proj_colls', 'action' => 'delete', $projColl['id']), array('confirm' => __('Are you sure you want to delete # %s?', $projColl['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Coll'), array('controller' => 'proj_colls', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
