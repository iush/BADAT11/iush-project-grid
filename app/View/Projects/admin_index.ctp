<div class="projects index">
	<h2><?php echo __('Projects'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('org_id'); ?></th>
			<th><?php echo $this->Paginator->sort('discipline_id'); ?></th>
			<th><?php echo $this->Paginator->sort('new_id'); ?></th>
			<th><?php echo $this->Paginator->sort('Teacher_Project_Posted_Sequence'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Type'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Essay'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Subject_Category_Tree'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Subject_Subcategory_Tree'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Grade_Level_Category'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Resource_Category'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Cost'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Posted_Date'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Current_Status'); ?></th>
			<th><?php echo $this->Paginator->sort('Project_Fully_Funded_Date'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projects as $project): ?>
	<tr>
		<td><?php echo h($project['Project']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($project['Org']['name'], array('controller' => 'orgs', 'action' => 'view', $project['Org']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($project['Discipline']['name'], array('controller' => 'disciplines', 'action' => 'view', $project['Discipline']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($project['New']['name'], array('controller' => 'news', 'action' => 'view', $project['New']['id'])); ?>
		</td>
		<td><?php echo h($project['Project']['Teacher_Project_Posted_Sequence']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Type']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['title']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Essay']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Subject_Category_Tree']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Subject_Subcategory_Tree']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Grade_Level_Category']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Resource_Category']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Cost']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Posted_Date']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Current_Status']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['Project_Fully_Funded_Date']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['created']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $project['Project']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $project['Project']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Orgs'), array('controller' => 'orgs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Org'), array('controller' => 'orgs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Disciplines'), array('controller' => 'disciplines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Discipline'), array('controller' => 'disciplines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List News'), array('controller' => 'news', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New New'), array('controller' => 'news', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Certs'), array('controller' => 'proj_certs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Cert'), array('controller' => 'proj_certs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Colls'), array('controller' => 'proj_colls', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Coll'), array('controller' => 'proj_colls', 'action' => 'add')); ?> </li>
	</ul>
</div>
