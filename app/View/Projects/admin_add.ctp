<div class="projects form">
<?php echo $this->Form->create('Project'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Project'); ?></legend>
	<?php
		echo $this->Form->input('org_id');
		echo $this->Form->input('discipline_id');
		echo $this->Form->input('new_id');
		echo $this->Form->input('Teacher_Project_Posted_Sequence');
		echo $this->Form->input('Project_Type');
		echo $this->Form->input('title');
		echo $this->Form->input('Project_Essay');
		echo $this->Form->input('Project_Subject_Category_Tree');
		echo $this->Form->input('Project_Subject_Subcategory_Tree');
		echo $this->Form->input('Project_Grade_Level_Category');
		echo $this->Form->input('Project_Resource_Category');
		echo $this->Form->input('Project_Cost');
		echo $this->Form->input('Project_Posted_Date');
		echo $this->Form->input('Project_Current_Status');
		echo $this->Form->input('Project_Fully_Funded_Date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Orgs'), array('controller' => 'orgs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Org'), array('controller' => 'orgs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Disciplines'), array('controller' => 'disciplines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Discipline'), array('controller' => 'disciplines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List News'), array('controller' => 'news', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New New'), array('controller' => 'news', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Certs'), array('controller' => 'proj_certs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Cert'), array('controller' => 'proj_certs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Colls'), array('controller' => 'proj_colls', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Coll'), array('controller' => 'proj_colls', 'action' => 'add')); ?> </li>
	</ul>
</div>
