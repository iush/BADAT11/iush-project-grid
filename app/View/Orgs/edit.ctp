<div class="orgs form">
<?php echo $this->Form->create('Org'); ?>
	<fieldset>
		<legend><?php echo __('Edit Org'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('type');
		echo $this->Form->input('percentage_Free_Lunch');
		echo $this->Form->input('state');
		echo $this->Form->input('zip');
		echo $this->Form->input('city');
		echo $this->Form->input('county');
		echo $this->Form->input('district');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Org.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Org.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Orgs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
	</ul>
</div>
