<div class="orgs index">
	<h2><?php echo __('Orgs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th class="actions"><?php echo __('Actions'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('percentage_Free_Lunch'); ?></th>
			<th><?php echo $this->Paginator->sort('state'); ?></th>
			<th><?php echo $this->Paginator->sort('zip'); ?></th>
			<th><?php echo $this->Paginator->sort('city'); ?></th>
			<th><?php echo $this->Paginator->sort('county'); ?></th>
			<th><?php echo $this->Paginator->sort('district'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($orgs as $org): ?>
	<tr>


		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $org['Org']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $org['Org']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $org['Org']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $org['Org']['id']))); ?>
		</td>


		<td><?php echo h($org['Org']['name']); ?>&nbsp;</td>
		<td><?php echo h($org['Org']['type']); ?>&nbsp;</td>
		<td><?php echo h($org['Org']['percentage_Free_Lunch']); ?>&nbsp;</td>
		<td><?php echo h($org['Org']['state']); ?>&nbsp;</td>
		<td><?php echo h($org['Org']['zip']); ?>&nbsp;</td>
		<td><?php echo h($org['Org']['city']); ?>&nbsp;</td>
		<td><?php echo h($org['Org']['county']); ?>&nbsp;</td>
		<td><?php echo h($org['Org']['district']); ?>&nbsp;</td>


	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Org'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
	</ul>
</div>
