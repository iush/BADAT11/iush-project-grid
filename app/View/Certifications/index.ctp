<div class="certifications index">
	<h2><?php echo __('Certifications'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('Certification_entity'); ?></th>
			<th><?php echo $this->Paginator->sort('Certificated_Posted_Date'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($certifications as $certification): ?>
	<tr>
		<td><?php echo h($certification['Certification']['id']); ?>&nbsp;</td>
		<td><?php echo h($certification['Certification']['name']); ?>&nbsp;</td>
		<td><?php echo h($certification['Certification']['Certification_entity']); ?>&nbsp;</td>
		<td><?php echo h($certification['Certification']['Certificated_Posted_Date']); ?>&nbsp;</td>
		<td><?php echo h($certification['Certification']['created']); ?>&nbsp;</td>
		<td><?php echo h($certification['Certification']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $certification['Certification']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $certification['Certification']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $certification['Certification']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $certification['Certification']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Certification'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Proj Certs'), array('controller' => 'proj_certs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Cert'), array('controller' => 'proj_certs', 'action' => 'add')); ?> </li>
	</ul>
</div>
