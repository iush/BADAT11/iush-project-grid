<div class="certifications view">
<h2><?php echo __('Certification'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($certification['Certification']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($certification['Certification']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Certification Entity'); ?></dt>
		<dd>
			<?php echo h($certification['Certification']['Certification_entity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Certificated Posted Date'); ?></dt>
		<dd>
			<?php echo h($certification['Certification']['Certificated_Posted_Date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($certification['Certification']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($certification['Certification']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Certification'), array('action' => 'edit', $certification['Certification']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Certification'), array('action' => 'delete', $certification['Certification']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $certification['Certification']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Certifications'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Certification'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Certs'), array('controller' => 'proj_certs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Cert'), array('controller' => 'proj_certs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Proj Certs'); ?></h3>
	<?php if (!empty($certification['ProjCert'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Certification Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($certification['ProjCert'] as $projCert): ?>
		<tr>
			<td><?php echo $projCert['id']; ?></td>
			<td><?php echo $projCert['name']; ?></td>
			<td><?php echo $projCert['project_id']; ?></td>
			<td><?php echo $projCert['certification_id']; ?></td>
			<td><?php echo $projCert['created']; ?></td>
			<td><?php echo $projCert['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proj_certs', 'action' => 'view', $projCert['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proj_certs', 'action' => 'edit', $projCert['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proj_certs', 'action' => 'delete', $projCert['id']), array('confirm' => __('Are you sure you want to delete # %s?', $projCert['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Cert'), array('controller' => 'proj_certs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
