<div class="disciplines view">
<h2><?php echo __('Discipline'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($discipline['Discipline']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($discipline['Discipline']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($discipline['Discipline']['description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Discipline'), array('action' => 'edit', $discipline['Discipline']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Discipline'), array('action' => 'delete', $discipline['Discipline']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $discipline['Discipline']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Disciplines'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Discipline'), array('action' => 'add')); ?> </li>
	</ul>
</div>
