<div class="projCerts view">
<h2><?php echo __('Proj Cert'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projCert['ProjCert']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($projCert['ProjCert']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projCert['Project']['title'], array('controller' => 'projects', 'action' => 'view', $projCert['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Certification'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projCert['Certification']['name'], array('controller' => 'certifications', 'action' => 'view', $projCert['Certification']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($projCert['ProjCert']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($projCert['ProjCert']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proj Cert'), array('action' => 'edit', $projCert['ProjCert']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Proj Cert'), array('action' => 'delete', $projCert['ProjCert']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $projCert['ProjCert']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Certs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Cert'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Certifications'), array('controller' => 'certifications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Certification'), array('controller' => 'certifications', 'action' => 'add')); ?> </li>
	</ul>
</div>
