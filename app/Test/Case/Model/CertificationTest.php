<?php
App::uses('Certification', 'Model');

/**
 * Certification Test Case
 */
class CertificationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.certification',
		'app.proj_cert',
		'app.project',
		'app.org',
		'app.new',
		'app.discipline',
		'app.collaborator',
		'app.proj_coll'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Certification = ClassRegistry::init('Certification');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Certification);

		parent::tearDown();
	}

}
