<?php
App::uses('ProjCert', 'Model');

/**
 * ProjCert Test Case
 */
class ProjCertTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.proj_cert',
		'app.project',
		'app.org',
		'app.collaborator',
		'app.proj_coll',
		'app.certification'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjCert = ClassRegistry::init('ProjCert');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjCert);

		parent::tearDown();
	}

}
