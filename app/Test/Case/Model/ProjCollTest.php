<?php
App::uses('ProjColl', 'Model');

/**
 * ProjColl Test Case
 */
class ProjCollTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.proj_coll',
		'app.project',
		'app.org',
		'app.collaborator'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjColl = ClassRegistry::init('ProjColl');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjColl);

		parent::tearDown();
	}

}
