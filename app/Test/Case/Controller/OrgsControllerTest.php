<?php
App::uses('OrgsController', 'Controller');

/**
 * OrgsController Test Case
 */
class OrgsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.org',
		'app.project',
		'app.collaborator',
		'app.proj_coll'
	);

}
