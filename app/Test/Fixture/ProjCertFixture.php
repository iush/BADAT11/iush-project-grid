<?php
/**
 * ProjCert Fixture
 */
class ProjCertFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'length' => 36, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'length' => 45),
		'project_id' => array('type' => 'string', 'null' => true, 'length' => 36),
		'certification_id' => array('type' => 'string', 'null' => true, 'length' => 36),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => true)
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '5dd486ae-11dc-40be-807e-5fe4d66a46af',
			'name' => 'Lorem ipsum dolor sit amet',
			'project_id' => 'Lorem ipsum dolor sit amet',
			'certification_id' => 'Lorem ipsum dolor sit amet',
			'created' => '2019-11-20 00:19:58',
			'modified' => '2019-11-20 00:19:58'
		),
	);

}
