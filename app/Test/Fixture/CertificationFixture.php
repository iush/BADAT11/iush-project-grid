<?php
/**
 * Certification Fixture
 */
class CertificationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'length' => 36, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'length' => 45),
		'Certification entity' => array('type' => 'text', 'null' => true),
		'Certificated_Posted_Date' => array('type' => 'datetime', 'null' => true),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => true)
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '5dd485e8-f36c-4e71-8095-5fe4d66a46af',
			'name' => 'Lorem ipsum dolor sit amet',
			'Certification entity' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'Certificated_Posted_Date' => '2019-11-20 00:16:40',
			'created' => '2019-11-20 00:16:40',
			'modified' => '2019-11-20 00:16:40'
		),
	);

}
