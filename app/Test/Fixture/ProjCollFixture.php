<?php
/**
 * ProjColl Fixture
 */
class ProjCollFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'length' => 36, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'length' => 45),
		'project_id' => array('type' => 'string', 'null' => true, 'length' => 36),
		'collaborator_id' => array('type' => 'string', 'null' => true, 'length' => 36),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => true)
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '5dd1e32c-597c-405a-b96c-421cd66a46af',
			'name' => 'Lorem ipsum dolor sit amet',
			'project_id' => 'Lorem ipsum dolor sit amet',
			'collaborator_id' => 'Lorem ipsum dolor sit amet',
			'created' => '2019-11-18 00:17:48',
			'modified' => '2019-11-18 00:17:48'
		),
	);

}
