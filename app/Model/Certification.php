<?php
App::uses('AppModel', 'Model');
/**
 * Certification Model
 *
 * @property ProjCert $ProjCert
 */
class Certification extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ProjCert' => array(
			'className' => 'ProjCert',
			'foreignKey' => 'certification_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
