<?php
App::uses('AppModel', 'Model');
/**
 * ProjCert Model
 *
 * @property Project $Project
 * @property Certification $Certification
 */
class ProjCert extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Certification' => array(
			'className' => 'Certification',
			'foreignKey' => 'certification_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
