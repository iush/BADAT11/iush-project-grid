# Database

```sql	
CREATE TABLE `projects` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	VARCHAR(45) NOT NULL,
	`created`	DATETIME,
	`modified`	DATETIME,
	`observations`	TEXT
);

CREATE TABLE "coor_projs" (
	id INTEGER NOT NULL, name VARCHAR(45), created DATETIME, modified DATETIME, observations TEXT, coordinators_id INTEGER DEFAULT 1 NOT NULL,
	CONSTRAINT coor_proj_PK PRIMARY KEY (id),
	CONSTRAINT coor_proj_coordinators_FK FOREIGN KEY (id) REFERENCES coordinators(id),
	CONSTRAINT coor_proj_projects_FK FOREIGN KEY (id) REFERENCES projects(id)
);

				
```

```sql
sqlite3 test.db .schema > schema.sql
```

php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
