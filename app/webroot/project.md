# Pgrid SQL

´´´sql
DB sqlite:project_grid.sqlite3 .tables
DB sqlite:project_grid.sqlite3 .schema projects
DB sqlite:project_grid.sqlite3 .schema disciplines
´´´

## Projects
´´´sql
DB sqlite:project_grid.sqlite3 SELECT * from projects
DB sqlite:project_grid.sqlite3 SELECT * from projects where (id == "0caf31d8ccc5f9c0b496c8443c7175c5")
DB sqlite:project_grid.sqlite3 SELECT COUNT(*) from projects
´´´

```sql
DB sqlite:project_grid.sqlite3 DROP table projects
DB sqlite:project_grid.sqlite3 CREATE TABLE `projects` ( 
	`id` VARCHAR(36) NOT NULL PRIMARY KEY, 
	`org_id` VARCHAR(36),
	`Teacher_Project_Posted_Sequence` INTEGER, 
	`Project_Type` TEXT, 
	`title` TEXT, 
	`Project_Essay` TEXT, 
	`Project_Subject_Category_Tree` TEXT, 
	`Project_Subject_Subcategory_Tree` TEXT, 
	`Project_Grade_Level_Category` TEXT, 
	`Project_Resource_Category` TEXT, 
	`Project_Cost` REAL, 
	`Project_Posted_Date` TEXT, 
	`Project_Current_Status` TEXT, 
	`Project_Fully_Funded_Date` TEXT,
        `created` DATETIME, 
	`modified` DATETIME,
       CONSTRAINT fk_org
           FOREIGN KEY (org_id)
           REFERENCES orgs(id)       
	)
